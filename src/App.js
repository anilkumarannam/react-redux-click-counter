import React from 'react';
import './App.css';
import ClickCounter from './components/ClickCounter';
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ClickCounter counterId={1}></ClickCounter>
        <ClickCounter counterId={2}></ClickCounter>
        <ClickCounter counterId={3}></ClickCounter>
      </header>
    </div>
  );
}

export default App;

