import { createSlice } from "@reduxjs/toolkit";

const clickCounterSlice = createSlice({
  name: "clickCounter",
  initialState: { value: {} },
  reducers: {
    increment: (state, actions) => {
      const { id } = actions.payload;
      if (id in state.value) {
        state.value[id] += 1;
      } else {
        state.value[id] = 1;
      }
    },
    decrement: (state, actions) => {
      const { id } = actions.payload;
      if (id in state.value) {
        state.value[id] -= 1;
      } else {
        state.value[id] = -1;
      }
    }
  }
});

export const { increment, decrement } = clickCounterSlice.actions;
export const selectClickCount = (state) => state.clickCounter.value;
export default clickCounterSlice.reducer;
