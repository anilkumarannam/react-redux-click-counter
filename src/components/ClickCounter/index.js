import { useDispatch, useSelector } from "react-redux";
import { decrement, increment, selectClickCount } from "./clickCounterSlice";

const ClickCounter = (props) => {
  const { counterId } = props;
  const count = useSelector(selectClickCount);
  const dispatch = useDispatch();
  return (
    <div>
      <h1>{count[counterId] || 0}</h1>
      <button onClick={() => dispatch(decrement({ id: counterId }))}>Decrease</button>
      <button onClick={() => dispatch(increment({ id: counterId }))}>Increase</button>
    </div>)
}

export default ClickCounter;
