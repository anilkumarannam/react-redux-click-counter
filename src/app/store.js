import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import clickCounterSlice from '../components/ClickCounter/clickCounterSlice';
export const store = configureStore({
  reducer: {
    counter: counterReducer,
    clickCounter: clickCounterSlice
  }
});
